//Lazyloading Hero Keen Slider images
var images = [
    "/assets/img/BSW_TimberHouse.jpg",
    "/assets/img/built-mockup.jpg",
    "assets/img/rsz_thecophouse-11-min.jpg",
    "assets/img/kit-of-parts.jpg",
]
var elements = document.querySelectorAll(".lazy__slide > img")
var loaded = []


//Easier Keen slider setup - just use HTML attr to setup slider
document.addEventListener( 'DOMContentLoaded', function () {
    window.jsSlider = []
    
    let allSliders = document.querySelectorAll('.slider-js');
    
    for (let index = 0, len = allSliders.length; index < len; index++ ) {
        let slider = allSliders[index]
        let progress = slider.querySelector('.progress')

        function updateClasses(instance, slider) {
          
            let slide = instance.details().relativeSlide
            let arrowLeft = slider.querySelector(".arrow-left")
            let arrowRight = slider.querySelector(".arrow-right")
            if(arrowRight){
                slide === 0 ? arrowLeft.classList.add("arrow--disabled") : arrowLeft.classList.remove("arrow--disabled")
            }
          
          
            if(arrowLeft){
                slide === instance.details().size - 1 ? arrowRight.classList.add("arrow--disabled") : arrowRight.classList.remove("arrow--disabled")
             }

            let dots = slider.querySelectorAll(".dot")
            if(dots){
                dots.forEach(function (dot, idx) {
                    idx === slide
                    ? dot.classList.add("dot--active")
                    : dot.classList.remove("dot--active")
                })
            }
            
        }
        
        let sliderInterval = 0;
        function autoplay(mainSlider, run) {
            clearInterval(sliderInterval);
            sliderInterval = setInterval(() => {
                if (run && mainSlider) {
                    mainSlider.next();
                }
            }, 5000);
        }
  
        let extraData = {
          created: function (instance) {
                  if(slider.querySelector(".arrow-left")){
                      slider.querySelector(".arrow-left")
                      .addEventListener("click", function () {
                            instance.prev()
                            if (progress) {
                                progress.classList.remove('active');
                                setTimeout(() => {
                                    progress.classList.add('active');
                                }, 50);
                            }
                      })
                    
                  }
                  if(slider.querySelector(".arrow-right")){
                    
                      slider.querySelector(".arrow-right")
                      .addEventListener("click", function () {
                            instance.next()
                            if (progress) {
                                progress.classList.remove('active');
                                setTimeout(() => {
                                    progress.classList.add('active');
                                }, 50);
                            }
                      })
                  }
            
                  let dots_wrapper = slider.querySelector(".dots")
                  
                if(dots_wrapper){
                    let slides = slider.querySelectorAll(".keen-slider__slide")
                    slides.forEach(function (t, idx) {
                        function isOdd(idx) { return idx % 2;}
                        if (isOdd()) return;
                        let dot = document.createElement("button")
                        dot.classList.add("dot")
                        dots_wrapper.appendChild(dot)
                        dot.addEventListener("click", function () {
                            instance.moveToSlide(idx)
                        })
                    })
                }

                   let counter_wrapper = slider.querySelector(".slides-indicator")

                   if(counter_wrapper){
                    let slides = slider.querySelectorAll(".keen-slider__slide");
                    let numCallbackRuns = 0;
                    slides.forEach(function (t, idx) {
                        numCallbackRuns++
                    })
                    document.getElementById("total-slides").textContent = numCallbackRuns;
                   }
                updateClasses(instance, slider)
            },

            afterChange: (s) => {
                const slideIdx = s.details().relativeSlide
                loaded[slideIdx] = true
                elements.forEach((element, idx) => {
                    if (loaded[idx]){ 
                        element.setAttribute("src", images[idx]);
                        element.classList.remove("lazy-hidden");
                    }
                })
            },
          
          slideChanged(instance) {
            updateClasses(instance, slider);
            
            let slides_indicator = slider.querySelector(".slides-indicator")
            if(slides_indicator){
                document.getElementById("current-slide").textContent = instance.details().relativeSlide + 1;
            }

            if(slider.dataset.slider){
              let dataSlider = JSON.parse(slider.dataset.slider)
              dataSlider = dataSlider;
              
              if(dataSlider.navFor){
                
                let slide = instance.details().relativeSlide;
                let size = instance.details().size;
                if(window.jsSlider[dataSlider.navFor]){
                   window.jsSlider[dataSlider.navFor].moveToSlide(slide)
                }
                
              }
            }
            
            
          }
        }
        
        if(slider.dataset.slider){
          let dataSlider = JSON.parse(slider.dataset.slider)
          
          dataSlider = dataSlider;
          dataSlider = {...dataSlider, ...extraData }
          

          if(dataSlider.progress && progress){
            progress.classList.add('active')
          }
          
          let autoPlayData = {
            dragStart: (e) => {
                autoplay(mainSlider, false)
            },
            dragEnd: (e) => {
                autoplay(mainSlider, true)
            }
          }
          
          if(dataSlider.autoplay){
            dataSlider = {...dataSlider, ...autoPlayData }
          }
          
          let selectSlider = slider.querySelector('.keen-slider');
          let mainSlider = new KeenSlider(selectSlider, dataSlider)

          if(dataSlider.autoplay){
              autoplay(mainSlider, true)
          }
          
         
          if(dataSlider.id){
              console.log("id: ", dataSlider.id);
              window.jsSlider[dataSlider.id] = mainSlider
           }
          
        }else{
          console.log("Please add the data-slider values")
        }
      
    }
  
    
});



//Get all Mobile menu elements and toggle classes onclick
var MenuButton = document.getElementById('main-menu-toggle');
var MenuNav = document.getElementById('main-nav');
var BodyOverflow = document.getElementById('page');

function MenuToggle(){
    MenuButton.classList.toggle("toggled");
    MenuNav.classList.toggle("toggled");
    BodyOverflow.classList.toggle("toggled");
}

MenuButton.onclick = function() {MenuToggle()};


const menuItems = document.querySelectorAll(".main-menu > .menu-item > .menu-link");

menuItems.forEach(function(menuItem) {
    menuItem.onclick = function() {
        MenuButton.classList.remove("toggled");
        MenuNav.classList.remove("toggled");
        BodyOverflow.classList.remove("toggled");
    };
  });


//Check screen sizes and remove / add toggle classes based on window width
window.addEventListener("resize", function() {
    if (window.innerWidth > 820){
        MenuButton.classList.remove("toggled");
        MenuNav.classList.remove("toggled");
        BodyOverflow.classList.remove("toggled");
    } 
    else if (window.innerWidth < 820){
        let menuItems = MenuNav.querySelectorAll(".menu-link")
        if(menuItems){
            menuItems.forEach(function (menuItem) {
                menuItem.onclick = function() {
                    MenuButton.classList.toggle("toggled");
                    MenuNav.classList.toggle("toggled");
                    BodyOverflow.classList.toggle("toggled");
                };
            })
        }
    }
});



//Rotation animation as user scrolls
var serviceLines = document.getElementById("service-tree-lines");
var cop26Lines = document.getElementById("cop26-tree-lines");
var multiplier = 0.025;

;(function() {
    var throttle = function(type, name, obj) {
        var obj = obj || window;
        var running = false;
        var func = function() {
            if (running) { return; }
            running = true;
            requestAnimationFrame(function() {
                obj.dispatchEvent(new CustomEvent(name));
                running = false;
            });
        };
        obj.addEventListener(type, func);
    };
    throttle ("scroll", "optimizedScroll");
})();

window.addEventListener("optimizedScroll", function() {
    serviceLines.style.transform = "rotate("+window.pageYOffset*multiplier+"deg)";
    cop26Lines.style.transform = "rotate("+window.pageYOffset*multiplier+"deg)";
});



//GSAP / ScrollMagic setup for house animation
document.addEventListener('DOMContentLoaded', () => {
    let controller = new ScrollMagic.Controller();

    let t4 = gsap.timeline();
    t4
        .to('.section_4_01', 4, {
            autoAlpha: 0
        })
        .from('.section_4_02', 4, {
            autoAlpha: 0
        }, '-=4')
        .from('.section_4_03', 4, {
            autoAlpha: 0
        })
        .from('.section_4_04', 4, {
            autoAlpha: 0
        })

    let scene4 = new ScrollMagic.Scene({
        triggerElement: '.forth-section',
        duration: '175%',
        triggerHook: 0,
        offset: '0'
    })
        .setTween(t4)
        .setPin('.forth-section')
        .addTo(controller);
})


//Observer for the class .transform and add .transformed when each .transform element comes into view - for easy/simple entrace animation
window.addEventListener('DOMContentLoaded', () => {
    if(!!window.IntersectionObserver){
        let observer = new IntersectionObserver((entries, observer) => { 
            entries.forEach(entry => {
            if(entry.isIntersecting){
                console.log(entry);
                entry.target.classList.add("transformed");
                observer.unobserve(entry.target);
            }
            });
        }, {rootMargin: "0px 0px -15% 0px"});
        document.querySelectorAll('.transform').forEach(test => { observer.observe(test) });
    }
    
    else document.querySelector('#warning').style.display = 'block';
});


// Getting the real VH and on resize - helps on mobile - use in css sparingly
window.addEventListener('resize', () => {
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
});




//Add .sticky class to header
window.onscroll = function() {myFunction()};

var header = document.getElementById("masthead");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}




//Add .hide class to the header when scrolling down and remove when scrolling up
(function(){

    var doc = document.documentElement;
    var w = window;
  
    var prevScroll = w.scrollY || doc.scrollTop;
    var curScroll;
    var direction = 0;
    var prevDirection = 0;
  
    var header = document.getElementById('masthead');
  
    var checkScroll = function() {
  
      /*
      ** Find the direction of scroll
      ** 0 - initial, 1 - up, 2 - down
      */
  
      curScroll = w.scrollY || doc.scrollTop;
      if (curScroll > prevScroll) { 
        //scrolled up
        direction = 2;
      }
      else if (curScroll < prevScroll) { 
        //scrolled down
        direction = 1;
      }
  
      if (direction !== prevDirection) {
        toggleHeader(direction, curScroll);
      }
  
      prevScroll = curScroll;
    };
  
    var toggleHeader = function(direction, curScroll) {
      if (direction === 2 && curScroll > 71) { 
  
        header.classList.add('hide');
        prevDirection = direction;
      }
      else if (direction === 1) {
        header.classList.remove('hide');
        prevDirection = direction;
      }
    };
  
    window.addEventListener('scroll', checkScroll);
  
  })();